(ns contacts.handler-test
  (:require [expectations :refer :all]
            [contacts.core :refer :all]
            [contacts.core-test :refer :all]
            [contacts.handler :refer :all]
            [datomic.api :as d])
  (:use ring.mock.request))

;;-----Test for DB existence and insert of single owner and contact
(expect #{["Sally"]}
        (with-redefs [conn (create-empty-in-memory-db)]
        (do
          (add-owner "Jon")
          (add-contact "Sally" "Jon")
          (find-contacts-for-owner "Jon"))))


;; ---------Test for REST interactions-----------
(expect 200
      (with-redefs [conn (create-empty-in-memory-db)]
        (do
          (add-owner "Jon")
          (let [response (app (request :get "/owners"))]
          (:status response)))))

(expect "[{\"owner\":[\"Jon\"]}]"
      (with-redefs [conn (create-empty-in-memory-db)]
        (do
          (add-owner "Jon")
          (let [response (app (request :get "/owners"))]
          (:body response)))))

(expect 404
    (let [response (app (request :get "/invalid"))]
      (:status response)))

(expect "Not Found"
    (let [response (app (request :get "/invalid"))]
      (:body response)))

#_(expect #{["Jon"] ["Jim"]}
        (let [response (app (request :get "/"))]
          (:body response)))

