(ns contacts.core-test
  (:require [expectations :refer :all]
            [contacts.core :refer :all]
            [datomic.api :as d]))

(defn create-empty-in-memory-db []
  (let [uri "datomic:mem://contacts-test-db"]
    (d/delete-database uri)
    (d/create-database uri)
    (let [conn (d/connect uri)
          schema (load-file "resources/datomic/schema.edn")]
      (d/transact conn schema)
      conn)))

;; ----------BEGIN DB Tests-----------

;;Test for DB existence and insert of single owner and contact
(expect #{["Sally"]}
        (with-redefs [conn (create-empty-in-memory-db)]
        (do
          (add-owner "Jon")
          (add-contact "Sally" "Jon")
          (find-contacts-for-owner "Jon"))))

;;Multiple owners (may or may not have contacts)
(expect '(["Jon"] ["Tiemann"])
        (with-redefs [conn (create-empty-in-memory-db)]
           (do
             (add-owner "Jon")
             (add-owner "Tiemann")
             (find-all-owners))))

;;Multiple contacts
(expect
 #_ #{["Jon"] ["Jim"]}
 #{["li'l jim"] ["slim"]}
         (with-redefs [conn (create-empty-in-memory-db)]
           (do
             (add-owner "Jon")
             (add-owner "Jim")
             (add-contact "slim" "Jon")
             (add-contact "li'l jim" "Jon")
             #_(find-all-owners)
             (find-contacts-for-owner "Jon"))))

;;one owner of one contact
(expect #{["jenny"]}
        (with-redefs [conn (create-empty-in-memory-db)]
        (do
          (add-owner "Jon")
          (add-contact "jenny" "Jon")
          (find-contacts-for-owner "Jon"))))

;;many owners of many pets
(expect #{["salty"] ["bill"]}
        (with-redefs [conn (create-empty-in-memory-db)]
        (do
          (add-owner "Jon")
          (add-owner "Ed")
          (add-contact "jenny" "Jon")
          (add-contact "jinny" "Jon")
          (add-contact "salty" "Ed")
          (add-contact "bill" "Ed")
          (find-contacts-for-owner "Ed"))))

;;----------------------
(expect #{["nice"]}
        (with-redefs [conn (create-empty-in-memory-db)]
         (do
           (add-owner "Jon")
           (add-contact "biggy" "Jon")
           (add-note "nice" "biggy")
           (find-notes-for-contact "biggy"))))
;;----------------------

(expect nil
        (with-redefs [conn (create-empty-in-memory-db)]
         (do
           (add-owner "Jon")
           (add-contact "biggy" "Jon")
           (add-note "nice" "biggy")
           (retract-owner "Jon"))))

(expect '(["Jon" "twiggy"])
        (with-redefs [conn (create-empty-in-memory-db)]
         (do
           (add-owner "Jon")
           (add-contact "biggy" "Jon")
           (add-contact "twiggy" "Jon")
           (add-note "nice" "biggy")
           (retract-contact "biggy")
           (find-all-contacts))))

(expect ()
        (with-redefs [conn (create-empty-in-memory-db)]
         (do
           (add-owner "Jon")
           (add-contact "biggy" "Jon")
           (add-contact "twiggy" "Jon")
           (add-note "nice" "biggy")
           (retract-contact "biggy")
           (find-all-notes))))

(expect ()
        (with-redefs [conn (create-empty-in-memory-db)]
         (do
           (add-owner "Jon")
           (add-contact "biggy" "Jon")
           (add-contact "twiggy" "Jon")
           (add-note "nice" "biggy")
           (retract-note "nice" "biggy")
           (find-all-notes))))


;; ;; ----------END DB Tests-----------


  ;;(add-owner "Jim")
  ;;(map (fn [o]{:owner o})  (find-all-owners))












