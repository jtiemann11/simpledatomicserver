  (ns contacts.core
    (:require [datomic.api :as d])
    (:use clojure.pprint))

  ;; --------DB Connect--------
  ;;(def uri  "datomic:mem://contacts-db")
  (def uri  "datomic:free://localhost:4334/contacts-db")

  ;; create database once per server execute (gone after server gone)
  (d/create-database uri)

  ;; connect to database
  (def conn (d/connect uri))

  ;; parse schema edn file
  (def schema-tx (read-string (slurp "resources/datomic/schema.edn")))

  ;; submit schema transaction
  @(d/transact conn schema-tx)


  ;; --------DB Helpers-------

  (defn find-contact-owner-id [owner-name]
    (ffirst (d/q '[:find ?eid
           :in $ ?owner-name
           :where [?eid :owner/name ?owner-name]]
         (d/db conn)
         owner-name)))

  (defn find-contact-id [contact-name]
    (ffirst (d/q '[:find ?eid
           :in $ ?contact-name
           :where [?eid :contact/name ?contact-name]]
         (d/db conn)
         contact-name)))

  (defn find-note-id [note-content contact-name]
    (ffirst (d/q '[:find ?eid
                    :in $ ?note-content ?contact-name
                    :where
                    [?e :contact/name ?contact-name]
                    [?e :contact/notes ?eid]
                    [?eid :note/content ?note-content]]
         (d/db conn)
         note-content contact-name)))

  ;; --------DB Queries-------

  (defn add-owner [owner-name]
    @(d/transact conn [{:db/id (d/tempid :db.part/user)
                      :owner/name owner-name}]))

  (defn add-contact [contact-name owner-name]
    (let [contact-id (d/tempid :db.part/user)]
                  @(d/transact conn [{:db/id contact-id
                                      :contact/name contact-name}
                                     {:db/id (find-contact-owner-id owner-name)
                                       :owner/contacts contact-id}])))


  (defn add-note [note-content contact-name]
    (println note-content contact-name)
    (let [contact-note-id (d/tempid :db.part/user)]
                  @(d/transact conn [{:db/id contact-note-id
                                      :note/content note-content}
                                     {:db/id (find-contact-id contact-name)
                                       :contact/notes contact-note-id}
                                     {:db/id (d/tempid :db.part/tx)
                                       :created/at (java.util.Date.)}])))


  (defn find-all-owners []
    (sort (d/q '[:find ?owner-name
        :where [_ :owner/name ?owner-name] ]
        (d/db conn))))

  (defn find-all-contacts []
    (sort (d/q '[:find ?owner-name ?contact-name
           :in $
           :where [?eid :owner/name ?owner-name]
                  [?eid :owner/contacts ?contact]
                  [?contact :contact/name ?contact-name]]
           (d/db conn)
           )))

  (defn find-all-notes []
    (sort (d/q '[:find ?note-content ?contact-name
           :in $
           :where [?eid :contact/name ?contact-name]
                  [?eid :contact/notes ?contact]
                  [?contact :note/content ?note-content]]
           (d/db conn)
           )))

  ;;find all owners/contacts/notes
  (defn find-all []
    (sort (d/q '[:find ?nco ?owner ?contact ?note
           :in $
           :where
     [?oe :owner/name ?owner]
     [?oe :owner/contacts ?oc]
     [?oc :contact/name ?contact]
     [?oc :contact/notes ?nco]
     [?nco :note/content ?note]]
         (d/db conn))))

  (defn find-contacts-for-owner [owner-name]
    (d/q '[:find ?contact-name
           :in $ ?owner-name
           :where [?eid :owner/name ?owner-name]
                  [?eid :owner/contacts ?contact]
                  [?contact :contact/name ?contact-name]]
           (d/db conn)
           owner-name))

  (defn find-notes-for-contact [contact-name]
      (d/q '[:find ?note-content
           :in $ ?contact-name
           :where [?eid :contact/name ?contact-name]
                  [?eid :contact/notes ?note]
                  [?note :note/content ?note-content]]
           (d/db conn)
           contact-name))


   (defn retract-owner [owner-name]
    @(d/transact conn [[:db.fn/retractEntity (find-contact-owner-id owner-name)]])
     (find-contact-owner-id owner-name))

    (defn retract-contact [contact-name]
    @(d/transact conn [[:db.fn/retractEntity (find-contact-id contact-name)]])
     (find-contact-id contact-name))

    (defn retract-note [note-content contact-name]
    @(d/transact conn [[:db.fn/retractEntity (find-note-id note-content contact-name)]])
     (find-note-id note-content contact-name))


;;----------------------Notes---------------------------------------
   ;;(find-all)
   ;;(find-note-id "huh" "biggy")
   ;;(retract-note "xoxo" "biggy")
    ;;(retract-owner "Jon")
;;     (find-all-owners)
;;     (find-all-contacts)
;;     (find-all-notes)
     ;;(find-all)
       ;; (add-note "sweet" "biggy")
        ;;(find-notes-for-contact "Colette")
  ;;(d/delete-database uri)
  ;; (def results (d/q '[:find ?c :where [?c :owner/name]] (d/db conn)))
  ;; (count results)

;;   (defn note-created-at [note-content contact-name]
;;      (first (d/entity (d/db conn) (:tx (first (d/datoms (d/db conn)
;;                                                :eavt  (find-note-id note-content contact-name) ))))))
;;   (note-created-at "Lunch?" "Tom" )
;;   (note-created-at "Lunch?" "John Loy" )

;;   (map #( :v %)  (d/datoms (d/db conn)  :eavt ))


;;      (partition 2 (interleave (map :tx  (d/datoms (d/db conn)  :eavt :note/content)) (find-all)))



  ;; (let [db (d/db conn)]
  ;;   (pprint (map #(let [entity (d/entity db (first %))]
  ;;                   [(:owner/name entity)
  ;;                    (-> entity :owner/contacts :contact/name)])
  ;;                results)))
