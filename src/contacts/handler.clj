 (ns contacts.handler
      (:use ring.util.response)
      (:use compojure.core)
      (:use cheshire.core)
      (:use contacts.core)
      (:require [ring.middleware.json :as middleware]
                [ring.util.response :as resp]
                [contacts.core :refer :all]
                [compojure.handler :as handler]
                [compojure.route :as route]
                [datomic.api :only [q db] :as d]))


(defn get-all-owners []
  #_(println (map (fn [o] {:owner o})  (find-all-owners)))
  (response (map (fn [o] {:owner o})  (find-all-owners))))

(defn get-all-contacts []
  (response (map (fn [o] {:owner (o 0) :contact (o 1)})  (find-all-contacts))))

(defn get-all-notes []
  (response (map (fn [o] {:owner (o 1) :contact (o 2) :note (o 3) :noteEId (o 0)})  (find-all))))

(defn post-retract-note [note-content contact-name]
  (response (retract-note note-content contact-name)))

    (defroutes app-routes
        (GET  "/owners" []  (get-all-owners) )
        (GET  "/contacts" []  (get-all-contacts) )
        (GET  "/notes" []  (get-all-notes) )
        (POST "/add_owner" {body :params} (add-owner (body :owner)))
        (POST "/add_contact"  {body :params} (add-contact (body :contact) (body :owner)))
        (POST "/add_note"  {body :params} (add-note (body :note) (body :contact)))
        (POST "/peanut" {body :params} (add-owner (body :owner))
              (resp/redirect "http://localhost/key/clo.html"))
        (POST "/delete_note" {body :params} (post-retract-note (body :content)(body :contact)))
      (route/not-found "Not Found"))


 (defn allow-cross-origin
  "middleware function to allow crosss origin"
  [handler]
  (fn [request]
    ;(println "REQUEST:" request)
   (let [response (handler request)]
   ; (println "RESPONSE:")
    (assoc response :headers (merge (:headers response) {"Access-Control-Allow-Origin" "*"}))
    #_(assoc response :headers (merge (:headers response) {"Access-Control-Allow-Methods" "GET, POST, PUT, DELETE, HEAD"}))
     )))


    (def app
        (-> (handler/api app-routes)
            (middleware/wrap-json-body)
            (middleware/wrap-json-response)
            (allow-cross-origin)))


