(defproject contacts "0.1.0-SNAPSHOT"
  :description "Contacts List with notes"
  :url "http://example.com/contacts"
  :main contacts.core
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [com.datomic/datomic-free "0.9.4331"]
                 ;;[com.datomic/datomic-pro "0.9.4360"]
                 [expectations "1.4.56"]
                 [ring "1.2.1"]
                 [ring/ring-json "0.2.0"]
                 [cheshire "4.0.3"]
                 ;;[ring-cors "0.1.0"]
                 [compojure "1.1.6"]]
  :plugins [[lein-ring "0.8.7"]]
  :ring {:handler contacts.handler/app
  ;;:middleware [#(wrap-cors % :access-control-allow-origin #"http://localhost"
    ;;                       :access-control-allow-headers ["Origin" "X-Requested-With"
      ;;                                                    "Content-Type" "Accept"])]
         :auto-reload? true
         :auto-refresh? false
         }
  :profiles
         {:dev {:dependencies [[ring-mock "0.1.5"]]}})

