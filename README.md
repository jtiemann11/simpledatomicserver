# Description
This is a server for contacts-notes.
It uses clojure (ring/compojure) and datomic.  
It was written to learn about integrating datomic and building a clojure based web api.
Also to learn the datomic's datalog, indexes and use of the console.
[I had just finished a lot of clojure koans and didn't see the use in making up business logic]

#Start

1. from datomic directory bin/transactor config/dev-transactor.properties
2. from contact directory lein ring server lein autoexpect (if you want to auto exec test)
Configs are: lein autoexpect in ~/.lein/profiles.clj
                            {:user 
                               {:plugins [[lein-autoexpect "1.0"]]}}

db uri (def uri "datomic:free://localhost:4334/contacts-db")

## Usage
Either build a front end or get my "simpledatomic" repo. [https://bitbucket.org/jtiemann11/simpledatomic]

An example of wiring to datomic backend thru a simple api, see simpleDatomic for client side html/js/css

## License

Copyright © 2013 Jon Tiemann

Distributed under the Eclipse Public License, the same as Clojure.
